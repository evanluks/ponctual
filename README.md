# Getting Started: Ponctual

## Sobre

*Ponctual* é um sistema de gerenciamento de jornadas de trabalho com batida de ponto.

Desenvolvido e mantido por Evanderson Lucas.

## Tecnologias e versões

  Ferramenta                  |  Versão
----------------------------- | --------
PHP                           | 8.1.*
Laravel                       | 9
Mysql                         | 8
Redis                         | alpine
mailhog/mailhog               | latest
minio/minio                   | latest

## Padrões de projeto

### MVC
Arquitetura MVC e tem como principal característica ajudar a desenvolver aplicações seguras e performáticas de forma rápida, com código limpo e simples, já que ele incentiva o uso de boas práticas de programação e utiliza o padrão PSR-2 como guia para estilo de escrita do código.

### SOLID
O SOLID é um princípio de design de software orientado a objeto (OOD). Basicamente, SOLID é um acrônimo para:

S - Single Responsibility Principle (Princípio da Responsabilidade Única);

O - Open-closed Principle (Princípio Aberto-fechado);

L - Liskov Substitution Principle (Princípio da Substituição de Liskov);

I - Interface Segregation Principle (Princípio da Segregação de Interface);

D - Dependency Inversion Principle (Princípio da Inversão de Dependência).

Para conhecer mais sobre o SOLID acesse [Princípios SOLID: o que são e como aplicá-los no PHP/Laravel](https://dev.to/lucascavalcante/principios-solid-o-que-sao-e-como-aplica-los-no-php-laravel-parte-01-responsabilidade-unica-3mjj).

### Service-Repository
O conceito de repositórios e serviços garante que você escreva código reutilizável e ajuda a manter seu controlador o mais simples possível, tornando-os mais legíveis.

### MySql

Container de bancos de dados da aplicação local, criada com base no ``` mysql:8 ```.

### Redis

Container de cache utilizado pelo horizon e filas, criada com base na ``` redis:alpine ```.

### Mailhog

[MailHog](https://github.com/mailhog/MailHog) é uma ferramenta de teste de e-mail para desenvolvedores, localmente é usada para o envio de emails. A imagem é criada com base na ``` mailhog/mailhog:latest ```.

Para acessar o mailhog, e ter acesso aos e-mails enviados, acesse [http://localhost:8025](http://localhost:8025).

### Minio

[MinIO](https://min.io) é um armazenamento de objetos de alto desempenho lançado sob a GNU Affero General Public License v3.0. É API compatível com o serviço de armazenamento em nuvem Amazon S3, localmente usa-se para o armazenamento de objetos que em produção são armazenadas na ``` AWS S3```. A imagem é criada com base na ``` minio/minio:latest ```.

Para configurar o MinIO, acesse a url [http://localhost:9090](http://localhost:9090), insira as credenciais que estão configuradas no docker-compose.yml, e crie um novo bucket, que deve ser o mesmo na configuração `AWS_BUCKET` do `.env`.

## Como executar

### Antes de executar

1 - Clone o repositório ```git clone git@bitbucket.org:evanluks/ponctual.git```.

2 - Acesse a pasta do projeto ```cd project-folder``` .

3 - Criar o .env ``` cp .env.example .env ```.

### Instale o Sail e suas dependências

    docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs

### Alias para Sail

1 - Abra .bashrc no vi ou vim ``` vi ~/.bashrc ```

2 - Na área de aliases adicione ``` alias sail='./vendor/bin/sail' ```

3 - Salve o arquivo
### Próximos passos

1 - Execute ``` sail up -d ``` para subir os containers.

2 - Execute ``` sail exec web bash ``` para entrar dentro do container do laravel.

3 - Dentro do container execute ``` composer install ``` ou ``` composer install --ignore-platform-reqs ``` para instalar as dependências.

4 - Gerar a APP_KEY ``` php artisan key:generate ```.

5 - Gerar as tabelas com migrations ``` php artisan migrate --seed ```.

6 - Para abrir o app acesse o link [http://localhost](http://localhost).

## Versionamento

Costuma-se seguir o fluxo de trabalho do [Git flow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow) para desenvolivento de novas features e/ou resolução de bugs.

O repositório conta com 2 branches padrão:

- master: branch padrão de produção;

- develop: branch de desenvolvimento;
