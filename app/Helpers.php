<?php

use Illuminate\Support\Facades\Date;
use Spatie\Html\Html;

if (!function_exists('collaborator')) {
    /**
     * Get current logged in collaborator.
     *
     * @return \App\Models\Customer|\Illuminate\Contracts\Auth\Authenticatable
     */
    function collaborator()
    {
        return auth('collaborators')->user();
    }
}

if (!function_exists('html')) {
    /**
     * Method for code conclusion.
     *
     * @return \App\Models\Customer|\Illuminate\Contracts\Auth\Authenticatable
     */
    function html()
    {
        return app(Html::class);
    }
}

if (! function_exists('camel_case')) {
    /**
     * Convert a value to camel case.
     *
     * @param  string  $value
     * @return string
     */
    function camel_case($value)
    {
        return Str::camel($value);
    }
}

if (!function_exists('__t')) {
    /**
     * A different approach to the `trans` method.
     *
     * @param string $key
     * @param string $fallback
     * @param array  $replace
     *
     * @return mixed
     */
    function __t($key, $fallback, $replace = [])
    {
        /** @var Translator $translator */
        $translator = trans();

        if ($translator->has($key, null)) {
            return $translator->get($key, $replace);
        }

        return $translator->get($fallback, $replace);
    }
}

if (!function_exists('modelAction')) {
    /**
     * Get some model action by type.
     *
     * @param string      $type
     * @param string      $action
     * @param string|null $fallback
     *
     * @return string
     */
    function modelAction($type, $action, $fallback = null)
    {
        return __t(
            "models.$type.actions.$action",
            $fallback
                ? "models.$type.actions.$fallback"
                : "models.default.actions.$action"
        );
    }
}

if (!function_exists('modelName')) {
    /**
     * Get some model attribute by type.
     *
     * @param string $type
     *
     * @return string
     */
    function modelName($type)
    {
        return __t(
            'models.'.$type.'.name',
            class_basename($type)
        );
    }
}

if (!function_exists('modelAttribute')) {
    /**
     * Get some model attribute by type.
     *
     * @param string      $type
     * @param string      $field
     * @param string|null $fallback
     *
     * @return string
     */
    function modelAttribute($type, $field, $fallback = null)
    {
        return __t(
            "models.$type.attributes.$field",
            $fallback
                ? "models.$type.attributes.$fallback"
                : "models.default.attributes.$field"
        );
    }
}

if (! function_exists('now')) {
    /**
     * Create a new Carbon instance for the current time.
     *
     * @param  \DateTimeZone|string|null  $tz
     * @return \Illuminate\Support\Carbon
     */
    function now($tz = null)
    {
        return Date::now($tz);
    }
}

if (! function_exists('config')) {
    /**
     * Get / set the specified configuration value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string|null  $key
     * @param  mixed  $default
     * @return mixed|\Illuminate\Config\Repository
     */
    function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('config');
        }

        if (is_array($key)) {
            return app('config')->set($key);
        }

        return app('config')->get($key, $default);
    }
}
