<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveCollaboratorRequest;
use App\Models\Collaborator;
use App\Repositories\CollaboratorRepository;
use App\Services\CollaboratorService;
use Illuminate\Http\Request;

class CollaboratorController extends CrudController
{
    /**
     * Type of the resource to manage.
     *
     * @var string
     */
    protected $resourceType = Collaborator::class;

    /**
     * Type of the managing repository.
     *
     * @var string
     */
    protected $repositoryType = CollaboratorRepository::class;

    /**
     * Returns the request that should be used to validate.
     *
     * @return Request
     */
    protected function formRequest()
    {
        return app(SaveCollaboratorRequest::class);
    }

    /**
     * Show the form for edit an existing resource.
     *
     * @param int $id
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Request $request, $id)
    {
        $instance = $this->getRepository()
            ->find($id, true);

        $instance->address = $instance->address;

        return $this->view('edit')
            ->with('type', $this->resourceType)
            ->with('instance', $instance);
    }

    /**
     * Where to redirect after creating the resource.
     *
     * @param Model $resource
     *
     * @return RedirectResponse
     */
    protected function afterCreate($resource)
    {
        return redirect(route('web.admin.collaborators.index'))
            ->with('success', 'Colaborador Criado com sucesso');
    }
}
