<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\HasForm;
use App\Http\Controllers\Admin\Concerns\HasModel;
use App\Http\Controllers\Admin\Concerns\HasViews;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    use HasForm;
    use HasModel;
    use HasViews;

    /**
     * Clean instance of the resource.
     *
     * @var Model
     */
    protected $instance;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->instance = $this->getRepository()
            ->getInstance();
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $resources = $this->getRepository()
            ->index($request->all());

        return $this->view('index')
            ->with('type', $this->resourceType)
            ->with('instance', $this->instance)
            ->with('resources', $resources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        return $this->view('create')
            ->with('type', $this->resourceType)
            ->with('instance', $this->instance)
            ->with('isUpdate', false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if ($resource = $this->getRepository()
            ->create($this->formParams())
        ) {
            return $this->afterCreate($resource);
        }

        return $this->afterFailed('created');
    }

    /**
     * Show the form for edit an existing resource.
     *
     * @param int $id
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Request $request, $id)
    {
        $instance = $this->getRepository()
            ->find($id, true);

        return $this->view('edit')
            ->with('type', $this->resourceType)
            ->with('instance', $instance)
            ->with('isUpdate', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $instance = $this->getRepository()
            ->find($id, true);

        if ($resource = $this->getRepository()
            ->update($instance, $this->formParams())
        ) {
            return $this->afterUpdate($resource);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(Request $request, $id)
    {
        $instance = $this->getRepository()
            ->find($id, true);

        if ($success = $this->getRepository()
            ->delete($instance)
        ) {
            return $this->afterDelete($instance);
        }

        return $this->afterFailed('deleted');
    }

    /**
     * Where to redirect after creating the resource.
     *
     * @param Model $resource
     *
     * @return RedirectResponse
     */
    protected function afterCreate($resource)
    {
        return $resource->route('index')->with(
            'success', 'Recurso criado com sucesso'
        );
    }

    /**
     * Where to redirect after updating resource.
     *
     * @param Model $resource
     *
     * @return RedirectResponse
     */
    protected function afterUpdate($resource)
    {
        return back()->with(
            'success', 'Recurso atualizado com sucesso'
        );
    }

    /**
     * Where to redirect after deleting resource.
     *
     * @param Model $resource
     *
     * @return RedirectResponse
     */
    protected function afterDelete($resource)
    {
        if ($resource->deleted_at) {
            return back()->with(
                'success', 'Recurso deletado com sucesso'
            );
        }

        return redirect()
            ->to($resource->route('index'))
            ->with(
                'success', 'Recurso deletado com sucesso'
            );
    }

    /**
     * Return with errors and message.
     *
     * @param string $action
     *
     * @return RedirectResponse
     */
    protected function afterFailed($action)
    {
        return back()
            ->withInput()
            ->with('warning', modelAction($this->resourceType, 'failed.' . $action));
    }
}
