<?php

namespace App\Http\Controllers\Admin;

use App\Models\Point;
use App\Repositories\PointRepository;
use App\Services\PointService;
use Illuminate\Http\Request;

class PointController extends CrudController
{
    /**
     * Type of the resource to manage.
     *
     * @var string
     */
    protected $resourceType = Point::class;

    /**
     * Type of the managing repository.
     *
     * @var string
     */
    protected $repositoryType = PointRepository::class;

    public function list(Request $request)
    {
        $start = $request->start ?? now()->startOfDay();
        $end = $request->end ?? now()->addDay()->endOfDay();

        $resources = (new PointService)->getAllPonits($start, $end);

        return view('admin.points.index')
            ->with('type', $this->resourceType)
            ->with('instance', $this->instance)
            ->with('resources', $resources);
    }
}
