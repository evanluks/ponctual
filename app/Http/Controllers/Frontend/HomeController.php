<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $lastPoint = collaborator()->points()->latest()->first();

        return view('frontend.home')
            ->with('latestPoint', $lastPoint ? $lastPoint->created_at->format('d/m/Y H:i:s') : "Sem pontos registrados");
    }
}
