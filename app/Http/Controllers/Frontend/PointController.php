<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\PointService;
use Illuminate\Http\Request;

class PointController extends Controller
{
    public function point()
    {
        if((new PointService)->hitPoint()) {
            return back()->with('success', 'Tudo certo, ponto registrado com sucesso!');
        }

        return back()->with('message', 'Erro ao bater ponto');
    }
}
