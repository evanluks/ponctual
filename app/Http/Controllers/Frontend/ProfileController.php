<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Show the form for editing current user
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit()
    {
        $instance = collaborator();

        return view('frontend.profile.edit')->with('instance', $instance);
    }

    /**
     * Update the current user.
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request)
    {
        $instance = collaborator();

        $this->validate($request, [
            'email' => ['unique:users,email,'.$instance->getKey()],
            'password' => $request->input('password') ? ['required', 'min:8'] : [],
            'password-confirmation' => $request->input('password') ? ['required', 'same:password'] : [],
        ]);

        $instance->name = $request->input('name');
        $instance->email = $request->input('email');

        if($request->input('password')) {
            $instance->password = bcrypt($request->input('password'));
        }

        $instance->save();

        return back()->with(
            'success', 'Dados alterados com sucesso!'
        );
    }
}
