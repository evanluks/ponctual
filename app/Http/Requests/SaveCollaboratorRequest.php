<?php

namespace App\Http\Requests;

use App\Models\Collaborator;
use Carbon\Carbon;

class SaveCollaboratorRequest extends CrudRequest
{
    /**
     * Type of class being validated.
     *
     * @var string
     */
    protected $type = Collaborator::class;

    /**
     * Rules when editing resource.
     *
     * @return array
     */
    protected function editRules()
    {
        return [
            'email' => ['required', 'email', 'unique:collaborators,email,' . $this->id, 'max:255'],
        ];
    }

    /**
     * Rules when creating resource.
     *
     * @return array
     */
    protected function createRules()
    {
        return [
            'password' => ['required'],
            'cpf' => ['required', 'cpf', 'unique:collaborators'],
            'email' => ['required', 'email', 'unique:collaborators', 'max:255'],
        ];
    }

    /**
     * Base rules for both creating and editing the resource.
     *
     * @return array
     */
    public function baseRules()
    {
        return [
            'name' => ['required', 'string', 'min:1', 'max:255'],
            'birth_date' => ['required', 'date'],
            'zip_code' => ['required', 'string', 'min:1', 'max:255'],
            'street' => ['required', 'string', 'min:1', 'max:255'],
            'number' => ['required', 'string', 'min:1', 'max:255'],
            'district' => ['required', 'string', 'min:1', 'max:255'],
            'state' => ['required', 'string', 'min:1', 'max:255'],
            'city' => ['required', 'string', 'min:1', 'max:255'],
        ];
    }
}
