<?php

namespace App\Models;

use App\Models\Relations\BelongsToCollaborator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory,
        SoftDeletes,
        BelongsToCollaborator;

    protected $fillable = ['collaborator_id', 'zip_code', 'street', 'number', 'district', 'state', 'city'];
}
