<?php

namespace App\Models;

use App\Models\Traits\HasRoutes;
use App\Models\Traits\AdminSearchable;
use App\Models\Relations\HasManyPoints;
use App\Models\Relations\HasOneAddress;
use App\Models\Traits\AdminOrderable;
use App\Models\Traits\HasColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Schema;

class Collaborator extends Authenticatable
{
    use HasFactory,
        SoftDeletes,
        Notifiable,
        HasManyPoints,
        HasOneAddress,
        HasColumns,
        HasRoutes,
        AdminSearchable,
        AdminOrderable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'name',
        'cpf',
        'email',
        'password',
        'role',
        'birth_date',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Executes filter on query.
     *
     * @param Builder $query
     * @param callable $filter
     * @return Builder
     */
    public function scopeFilter($query, $filter)
    {
        return call_user_func($filter, $query);
    }

    /**
     * Get all fillable attributes
     *
     * @return array
     */
    public function getAllFillable()
    {
        return Schema::getColumnListing($this->getTable());
    }

    /**
     * List of headers for the admin listing table.
     *
     * @return array
     */
    public function getAdminColumns()
    {
        return ['name', 'cpf', 'email', 'role', 'birth_date'];
    }
}
