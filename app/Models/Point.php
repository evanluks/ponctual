<?php

namespace App\Models;

use App\Models\Relations\BelongsToCollaborator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\HasColumns;
use App\Models\Traits\AdminSearchable;
use App\Models\Traits\AdminOrderable;
use App\Models\Traits\HasRoutes;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class Point extends Model
{
    use HasFactory,
        SoftDeletes,
        BelongsToCollaborator,
        HasColumns,
        HasRoutes,
        AdminSearchable,
        AdminOrderable;

    /**
     * Executes filter on query.
     *
     * @param Builder $query
     * @param callable $filter
     * @return Builder
     */
    public function scopeFilter($query, $filter)
    {
        return call_user_func($filter, $query);
    }

    /**
     * Get all fillable attributes
     *
     * @return array
     */
    public function getAllFillable()
    {
        return Schema::getColumnListing($this->getTable());
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'collaborator_id',
        'created_at',
    ];

    public function getCreatedAtAdminColumn()
    {
        return $this->created_at->format('d/m/Y H:i:s');
    }

    public function getCollaboratorIdAdminColumn()
    {
        return $this->collaborator->name;
    }

    /**
     * List of headers for the admin listing table.
     *
     * @return array
     */
    public function getAdminColumns()
    {
        return ['id', 'collaborator_id', 'created_at'];
    }
}
