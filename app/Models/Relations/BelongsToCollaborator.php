<?php

namespace App\Models\Relations;

use App\Models\Collaborator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToCollaborator
{
    /**
     * Represents a database relationship.
     *
     * @return BelongsTo|Builder|Collaborator
     */
    public function collaborator()
    {
        return $this->belongsTo(Collaborator::class);
    }
}
