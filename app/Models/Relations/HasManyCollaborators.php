<?php

namespace App\Models\Relations;

use App\Models\Collaborator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasManyCollaborators
{
    /**
     * Represents a database relationship.
     *
     * @return HasMany|Builder|Collaborator[]
     */
    public function collaborators()
    {
        return $this->hasMany(Collaborator::class);
    }
}
