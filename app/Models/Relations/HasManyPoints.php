<?php

namespace App\Models\Relations;

use App\Models\Point;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasManyPoints {

    /**
     * Represents a database relationship.
     *
     * @return HasMany|Builder|Point[]
     */
    public function points()
    {
        return $this->hasMany(Point::class);
    }
}
