<?php

namespace App\Repositories;

use App\Libraries\Viacep\AddressRepository;
use App\Models\Address;

class CollaboratorRepository extends CrudRepository
{
    /**
     * Type of the resource to manage.
     *
     * @var string
     */
    protected $resourceType = Collaborator::class;

    /**
     * Filter attributes.
     *
     * @param array $attributes
     *
     * @return array
     */
    public function filterAttributes($attributes)
    {
        // dd($attributes);
        if (is_null(data_get($attributes, 'email'))) {
            unset($attributes['email']);
        } else {
            $attributes['email'] = strtolower($attributes['email']);
        }

        if (is_null(data_get($attributes, 'password'))) {
            unset($attributes['password']);
        } else {
            $attributes['password'] = bcrypt($attributes['password']);
        }

        return $attributes;
    }

    /**
     * Handles model after save.
     *
     * @param Collaborator $resource
     * @param array    $attributes
     *
     * @return Collaborator
     */
    public function afterSave($resource, $attributes, $action = null)
    {
        $this->saveAddress($resource, $attributes);

        return $resource;
    }

    /**
     * Save address.
     *
     * @param Collaborator $collaborator
     * @param array    $attributes
     *
     * @return Address
     */
    public function saveAddress($collaborator, $attributes)
    {
        $address = $collaborator->address ?: new Address();

        /** @var Address $address */
        $address = $this->fill($address, $attributes, true);
        $address->collaborator_id = $collaborator->getKey();
        $address->save();

        // $this->checkIfZipcodeExists($address);

        return $address;
    }
}
