<?php

namespace App\Repositories;

class PointRepository extends CrudRepository
{
    /**
     * Type of the resource to manage.
     *
     * @var string
     */
    protected $resourceType = Point::class;
}
