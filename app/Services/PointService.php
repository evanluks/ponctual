<?php

namespace App\Services;

use App\Models\Point;
use App\Models\Points;
use Illuminate\Support\Facades\DB;

class PointService
{
    public function hitPoint()
    {
        return Point::create([
            'collaborator_id' => collaborator()->id,
        ]);
    }

    public function getAllPonits($start, $end)
    {
        return collect(DB::select(
            'select p.id, c.name, c.role, (YEAR(CURDATE())-YEAR(c.birth_date)) as age, u.name as manager_name, p.created_at as date_point from points p
            left join collaborators c on c.id = p.collaborator_id
            left join users u on c.user_id = u.id
            where p.created_at between ? and ?;',
            [$start, $end]
        ));
    }
}
