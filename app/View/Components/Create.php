<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Create extends Component
{
    public $route;
    public $validation;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($route, $validation)
    {
        $this->route = $route;
        $this->validation = $validation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('admin.layouts.partials.crud.create');
    }
}
