<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Collaborator;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CollaboratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collaborator = new Collaborator();
        $collaborator->user_id = 1;
        $collaborator->name = 'Colaborador Teste';
        $collaborator->cpf = '767.485.390-05';
        $collaborator->email = 'collaboratorteste@ponctual.com.br';
        $collaborator->password = bcrypt('senha321');
        $collaborator->role = 'Product Manager';
        $collaborator->birth_date = Carbon::parse('2000-08-23');
        $collaborator->save();

        $address = new Address();
        $address->collaborator_id = $collaborator->id;
        $address->zip_code = '59255-000';
        $address->state = 'RN';
        $address->city = 'Santo Antônio';
        $address->street = 'Salustiano Fagundes';
        $address->number = '60';
        $address->district = 'Centro';
        $address->save();
    }
}
