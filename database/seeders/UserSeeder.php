<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Usuário Administrador';
        $user->email = 'useradmin@ponctual.com.br';
        $user->password = bcrypt('senha123');
        $user->save();

        $user = new User();
        $user->name = 'Usuário Gestor';
        $user->email = 'manageuser@ponctual.com.br';
        $user->password = bcrypt('senha123');
        $user->save();
    }
}
