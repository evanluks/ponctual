<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Custom Models Attributes
    |--------------------------------------------------------------------------
    */

    'App\\Models\\Collaborator' => [
        'attributes' => [
            'name' => 'Nome',
            'cpf' => 'CPF',
            'email' => 'E-mail',
            'role' => 'Cargo',
            'birth_date' => 'Data de Nascimento',
            'created_at' => 'Criado em',
        ],
        'actions' => [
            'label' => 'Colaboradores',
            'index' => 'Lista de Colaboradores',
            'create' => 'Criar Colaborador',
            'edit' => 'Editar Colaborador',
            'delete' => 'Excluir Colaborador',
            'restore' => 'Restaurar Colaborador',
        ],
    ],

    'App\\Models\\Point' => [
        'attributes' => [
            'id' => 'ID',
            'collaborator_id' => 'Colaborador',
            'created_at' => 'Criado em',
        ],
        'actions' => [
            'label' => 'Pontos',
            'index' => 'Lista de Pontos',
        ],
    ],

    'default' => [
        'attributes' => [
            'password' => 'Senha',
        ],
        'actions' => [],
    ],
];
