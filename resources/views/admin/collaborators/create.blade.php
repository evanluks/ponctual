<x-app-layout>
    <div class="flex justify-between items-center mb-8 bg-white rounded shadow p-4 py-3">
        <span class="text-2xl">{{ modelAction($type, 'create') }}</span>
    </div>
    <x-create route="{{ route('web.admin.collaborator.store') }}" validation="{{ $instance->hasRoute('validation') }}">
        <x-slot name="form">
            @include('admin.collaborators.form')
        </x-slot>
    </x-create>
</x-app-layout>
