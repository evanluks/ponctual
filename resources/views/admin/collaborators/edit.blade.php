<x-app-layout>
    <div class="flex justify-between items-center mb-8 bg-white rounded shadow p-4 py-3">
        <span class="text-2xl">{{ modelAction($type, 'edit') }}</span>
    </div>
    <x-edit route="{{ route('web.admin.collaborator.update', $instance->id) }}" validation="{{ $instance->hasRoute('validation') ?? '' }}">
        <x-slot name="form">
            @include('admin.collaborators.form')
        </x-slot>
    </x-edit>
</x-app-layout>
