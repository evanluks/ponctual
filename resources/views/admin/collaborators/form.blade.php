
<div class='md:grid grid-cols-10 gap-x-8'>
    <div class='col-span-7 mb-6'>
        <div class='bg-white rounded shadow mb-5'>
            <div class='border-b p-2 pl-5'>
                <label class="text-xl">Informações Pessoais</label>
            </div>
            <div class="p-5">
                <x-input id="user_id" class="block mt-1 w-full" type="hidden" name="user_id" value="{{ Auth::user()->id }}" required autofocus />
                <div>
                    <x-label for="name" :value="__('Nome')" />
                    <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $instance->name }}" required autofocus />
                    @error('name')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="cpf" :value="__('CPF')" />
                    <x-input id="cpf" class="block mt-1 w-full" type="text" name="cpf" value="{{ $instance->cpf }}" required />
                    @error('cpf')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="email" :value="__('Email')" />
                    <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ $instance->email }}" required />
                    @error('email')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="role" :value="__('Cargo')" />
                    <x-input id="role" class="block mt-1 w-full" type="text" name="role" value="{{ $instance->role }}" required />
                    @error('role')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="birth_date" :value="__('Data de Nascimento')" />
                    <x-input id="birth_date" class="block mt-1 w-full" type="date" name="birth_date" value="{{ $instance->birth_date }}" required />
                    @error('birth_date')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class='bg-white rounded shadow mb-5'>
            <div class='border-b p-2 pl-5'>
                <label class="text-xl">Definição de Senha</label>
            </div>
            <div class="p-5">
                <div>
                    <x-label for="password" :value="__('Senha')" />
                    <x-input id="password" class="block mt-1 w-full" type="password" name="password" autofocus />
                    @error('password')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="confirm-password" :value="__('Confirmação de Senha')" />
                    <x-input id="confirm-password" class="block mt-1 w-full" type="password" name="confirm-password" autofocus />
                </div>
            </div>
        </div>
    </div>
    <div class="col-span-3 mb-6" x-data="address()">
        <div class="bg-white rounded shadow mb-5">
            <div class="border-b p-2 pl-5">
                <label class="text-xl">Endereço</label>
            </div>
            <div class="p-5">
                <div>
                    <x-label for="zip_code" :value="__('CEP')" />
                    <x-input id="zip_code" class="block mt-1 w-full" type="text" x-on:keyup="getAddress()" name="zip_code" value="{{ $instance->address->zip_code ?? '' }}" required x-model="zipcode" autofocus />
                    @error('zip_code')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="state" :value="__('Estado')" />
                    <x-input id="state" class="block mt-1 w-full" type="text" name="state" value="{{ $instance->address->state ?? '' }}" required x-model="state" />
                    @error('state')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="city" :value="__('Cidade')" />
                    <x-input id="city" class="block mt-1 w-full" type="text" name="city" value="{{ $instance->address->city ?? '' }}" required x-model="city" />
                    @error('city')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="street" :value="__('Rua')" />
                    <x-input id="street" class="block mt-1 w-full" type="text" name="street" value="{{ $instance->address->street ?? '' }}" required x-model="street" />
                    @error('street')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="number" :value="__('Número')" />
                    <x-input id="number" class="block mt-1 w-full" type="text" name="number" value="{{ $instance->address->number ?? '' }}" required x-model="number" />
                    @error('number')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
                <div class="mt-4">
                    <x-label for="district" :value="__('Bairro')" />
                    <x-input id="district" class="block mt-1 w-full" type="text" name="district" value="{{ $instance->address->district ?? '' }}" required x-model="district" />
                    @error('district')
                        <small class='mt-1 text-red-500'>{{ $message }}</small>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<input class="p-2 px-6 mt-6 bg-blue-500 text-white font-bold rounded shadow hover:bg-blue-600" type="submit" value="Salvar">

<script>
    function address() {
        return {
            zipcode: "{{ $instance->address->zip_code ?? null }}",
            street: "{{ $instance->address->street ?? null }}",
            number: "{{ $instance->address->number ?? null }}",
            state: "{{ $instance->address->state ?? null }}",
            city: "{{ $instance->address->city ?? null }}",
            district: "{{ $instance->address->district ?? null }}",
            getAddress() {
                let zipcode = this.zipcode.replace(/-/g, '');

                if (zipcode.length == 8) {
                    fetch(`https://viacep.com.br/ws/${this.zipcode}/json/`)
                        .then(response => response.json())
                        .then(response => {
                            this.state = response.uf ?? '';
                            this.city = response.localidade ?? '';
                            this.street = response.logradouro ?? '';
                            this.district = response.bairro ?? '';
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            }
        }
    }
</script>
