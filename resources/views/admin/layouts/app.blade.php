<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css"/>
        <script src="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"></script>
        <!-- or -->
        {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"/> --}}
        {{-- <script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script> --}}

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div>
            <div class="flex items-stretch bg-gray-200" x-data="{ sidebarOpen: window.innerWidth < 992 ? false : true, navigationOpen: false }">
                @include('admin.layouts.partials.sidebar')
                <div class="w-full min-h-screen">
                    @include('admin.layouts.navigation')
                    <!-- Page Content -->
                    <main class="lg:p-8 p-4">
                        {{ $slot }}
                    </main>
                </div>
            </div>

        </div>
    </body>
</html>
