<div>
    {{ html()->form('POST', $route)->acceptsFiles(true)->data('validation', $validation ? $validation : '')->open() }}
        {{ $form }}
    {{ html()->form()->close() }}
</div>
