<div class="flex justify-between items-center mb-8 bg-white rounded shadow p-4 py-2">
    <span class="text-2xl">{{ modelAction($type, 'label') }}</span>
    <a href="{{ route('web.admin.'.$instance->getTable().'.create') }}" class="p-2 px-6 bg-green-500 rounded shadow hover:bg-green-600">
        <span class="font-bold text-white">{{ modelAction($type, 'create') }}</span>
    </a>
</div>
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-left text-gray-800 bg-white">
        <thead class="text-sm text-gray-700 uppercase border-b">
            <tr>
                @foreach ($instance->getAdminColumns() as $column)
                    <th scope="col" class="px-6 py-3" {!! $instance->getAdminColumnAttributes($loop->index, $column) !!}>
                        {{ modelAttribute($type, $column) }}
                    </th>
                @endforeach
                <th scope="col" class="px-6 py-3">
                    Acões
                </th>
            </tr>
        </thead>
        <tbody>
        @forelse ($resources as $resource)
            <tr class="bg-white border-b" {!! $instance->getAdminRowAttributes($loop->index) !!}>
                @foreach ($resource->getAdminColumns() as $column)
                    {{-- @dump($resource->getAdminColumn($column)) --}}
                    <td scope="row" class="px-6 py-4 font-medium whitespace-nowrap" {!! $resource->getAdminColumnAttributes($loop->index, $column) !!}>
                        {!! $resource->getAdminColumn($column) !!}
                    </td>
                @endforeach
                <td class="px-6 py-4 flex space-x-1 font-medium whitespace-nowrap">
                    <button class="w-8 h-8 flex justify-center items-center rounded shadow p-1 bg-yellow-500 hover:bg-yellow-300">
                        <a href="{{ route('web.admin.collaborator.edit', $resource->id) }}" title="Editar" class="p-2">
                            <i class="fas fa-pen"></i>
                        </a>
                    </button>
                    <button class="w-8 h-8 flex justify-center items-center rounded shadow p-1 bg-red-500">
                        <a href="{{ route('web.admin.collaborator.delete', $resource->id) }}" class="p-2"
                            title="Excluir" data-method="DELETE"
                        >
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </button>
                </td>
            </tr>
        @empty
            <tr class="bg-white border-b">
                <td scope="row" class="px-6 py-4 font-medium whitespace-nowrap" colspan="{{ count($instance->getAdminColumns()) + 1 }}">
                    Nenhum registro no momento
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @if($pagination = $resources->appends(request()->all())->links())
        <div class="p-4">
            {!! $pagination !!}
        </div>
    @endif
</div>
