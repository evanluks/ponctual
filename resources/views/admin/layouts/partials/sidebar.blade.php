<div class="fixed md:relative transform md:transform-none duration-200 z-50" :class="{ 'translate-x-0 md:ml-0 ease-in': sidebarOpen, '-translate-x-full md:-ml-60 ease-out': !sidebarOpen }">
    <div class="relative w-screen md:w-60" data-simplebar>
        <div class="fixed min-h-screen max-h-screen w-full md:w-60 bg-gray-800 overflow-auto" data-simplebar>
            <div class="flex justify-between items-center bg-gray-700 mb-4">
                <a href="/dashboard">
                    <div class="flex space-x-4 items-center py-4 px-3">
                        <div class="shrink-0 flex items-center">
                            <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                        </div>

                        <div class="sm:-my-px sm:ml-4 sm:flex">
                            <span class="text-gray-100 text-xl">Ponctual</span>
                        </div>
                    </div>
                </a>

                <span x-on:click="{ sidebarOpen = !sidebarOpen }">
                    <i class="md:hidden fas fa-times text-xl text-gray-100 mr-4"></i>
                </span>
            </div>
            <div class="menu px-2 text-gray-200">
                <a href="{{ route('dashboard') }}">
                    <div class="flex items-center rounded-md p-2 py-4 transition hover:bg-gray-400 hover:text-gray-800 text-md">
                        <i class="w-6 my-1 mr-2 fas fa-chart-pie"></i>
                        <label>Dashboard</label>
                    </div>
                </a>
                <a href="{{ route('web.admin.collaborators.index') }}">
                    <div class="flex items-center rounded-md p-2 py-4 transition hover:bg-gray-400 hover:text-gray-800 text-md">
                        <i class="w-6 my-1 mr-2 fas fa-user"></i>
                        <label>Colaborador</label>
                    </div>
                </a>
                <a href="{{ route('web.admin.mycollaborators.index') }}">
                    <div class="flex items-center rounded-md p-2 py-4 transition hover:bg-gray-400 hover:text-gray-800 text-md">
                        <i class="w-6 my-1 mr-2 fas fa-location-dot"></i>
                        <label>Pontos</label>
                    </div>
                </a>
                {{-- <a href="{{ route('web.admin.points.index') }}">
                    <div class="flex items-center rounded-md p-2 py-4 transition hover:bg-gray-400 hover:text-gray-800 text-md">
                        <i class="w-6 my-1 mr-2 fa-solid fa-location-dot"></i>
                        <label>Pontos</label>
                    </div>
                </a> --}}
            </div>
        </div>
    </div>
</div>
