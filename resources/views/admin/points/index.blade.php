<x-app-layout>
    <div class="flex justify-between items-center mb-8 bg-white rounded shadow p-4 py-2">
        <span class="text-2xl">{{ modelAction($type, 'label') }}</span>
        <div class="flex justify-end">
            <div class="bg-white rounded shadow">
                <form action="{{ route('web.admin.mycollaborators.index') }}">
                    <div class="flex space-x-4">
                        <div>
                            <x-label for="start" :value="__('Data mínima')" />
                            <x-input id="start" class="block mt-1 h-8 w-full" type="date" name="start" />
                        </div>

                        <div>
                            <x-label for="end" :value="__('Data máxima')" />
                            <x-input id="end" class="block mt-1 h-8 w-full" type="date" name="end" />
                        </div>

                        <x-input id="filter" class="block w-36 bg-blue-500 text-white font-bold hover:bg-blue-600" type="submit" value="Filtrar" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-left text-gray-800 bg-white">
            <thead class="text-sm text-gray-700 uppercase border-b">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        ID
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nome
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Cargo
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Idade
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nome do Gestor
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Ponto
                    </th>
                </tr>
            </thead>
            <tbody>
            @forelse ($resources as $resource)
                <tr class="bg-white border-b">
                    @foreach ($resource as $column)
                        <td scope="row" class="px-6 py-4 font-medium whitespace-nowrap">
                            {!! $column !!}
                        </td>
                    @endforeach
                </tr>
            @empty
                <tr class="bg-white border-b">
                    <td scope="row" class="px-6 py-4 font-medium whitespace-nowrap" colspan="{{ count($instance->getAdminColumns()) + 1 }}">
                        Nenhum registro no momento
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</x-app-layout>
