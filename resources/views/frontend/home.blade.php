<x-frontend-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="mb-6">
                        Último ponto: {{ $latestPoint }}
                    </div>
                    <form method="POST" action="{{ route('web.frontend.hit-point') }}">
                        @csrf
                        <button class="bg-green-500 px-6 py-2 rounded shadow font-bold text-gray-100">
                            Registrar Ponto
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-frontend-layout>
