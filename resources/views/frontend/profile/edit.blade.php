<x-frontend-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <span class="text-2xl">Perfil de Usuário</span>
                </div>
            </div>
        </div>
    </div>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <form method="POST" action="{{ route('web.frontend.update') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="name" :value="__('Nome')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $instance->name }}" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ $instance->email }}" autofocus />
            </div>

            <hr class="mt-8" />

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Senha')" />

                <x-input id="password" class="block mt-1 w-full" type="password" name="password" autocomplete="current-password" />
            </div>

            <div class="mt-4">
                <x-label for="password-confirmation" :value="__('Confirmação de Senha')" />

                <x-input id="password-confirmation" class="block mt-1 w-full" type="password" name="password-confirmation" autocomplete="current-password" />
            </div>

            <div class="flex justify-end items-center block mt-6">
                <x-button>{{ __('Salvar') }}</x-button>
            </div>
        </form>
    </div>
</x-frontend-layout>
