<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CollaboratorController;
use App\Http\Controllers\Admin\PointController;
use Illuminate\Support\Facades\Route;

// Admin routes -------------------------------------------------------------------------------------------
Route::get('admin', function () {
    return redirect()->route('web.admin.login');
});

Route::get('/dashboard', function () {
    return view('admin.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::prefix('admin')->group(function () {
    Route::get('login', [LoginController::class, 'create'])->name('web.admin.login');
    Route::post('login', [LoginController::class, 'store'])->name('admin.login');

    Route::get('/collaborators', [CollaboratorController::class, 'index'])->name('web.admin.collaborators.index');
    Route::get('/collaborators/create', [CollaboratorController::class, 'create'])->name('web.admin.collaborators.create');
    Route::post('/collaborators', [CollaboratorController::class, 'store'])->name('web.admin.collaborator.store');
    Route::get('/collaborators/{id}/edit', [CollaboratorController::class, 'edit'])->name('web.admin.collaborator.edit');
    Route::put('/collaborators/{id}', [CollaboratorController::class, 'update'])->name('web.admin.collaborator.update');
    Route::get('/collaborators/{id}', [CollaboratorController::class, 'delete'])->name('web.admin.collaborator.delete');

    Route::get('/points', [PointController::class, 'list'])->name('web.admin.mycollaborators.index');

    Route::middleware('auth')->group(function () {
        Route::post('logout', [LoginController::class, 'destroy'])->name('web.admin.logout');
    });
});

