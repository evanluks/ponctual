<?php

use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\PointController;
use App\Http\Controllers\Frontend\ProfileController;
use App\Models\Collaborator;
use Illuminate\Support\Facades\Route;

// Frontend routes ----------------------------------------------------------------------------------------

Route::get('login', [LoginController::class, 'create'])->name('web.frontend.login');
Route::post('login', [LoginController::class, 'store'])->name('frontend.login');


Route::middleware('auth:collaborators')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('web.frontend.home');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('web.frontend.edit');
    Route::post('/profile', [ProfileController::class, 'update'])->name('web.frontend.update');

    Route::post('logout', [LoginController::class, 'destroy'])->name('web.frontend.logout');

    Route::post('hit-point', [PointController::class, 'point'])->name('web.frontend.hit-point');
});
